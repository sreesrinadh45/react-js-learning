import React from 'react'
import { Link } from 'react-router-dom';


function Navigation() {
  return (
    <div className='navbar'>
      <ul className='nav-links'>
        <Link to='/Home'>Home</Link>
        <Link to='/ContactUs'>ContactUs</Link>
        <Link to='/AboutUs'>AboutUs</Link>
        <Link to='/Card'>Card</Link>
        <Link to='/ReactTable'>ReactTable</Link>
      </ul>
    </div>
  )
}
export default Navigation;
