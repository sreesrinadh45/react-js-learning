
import './App.css';

import Navigation from './Navigation';
import {  Route, Routes,BrowserRouter, } from 'react-router-dom';
//import UseRefINClass from './UseRefINClass';
import Home from './Home';
import ContactUs from './ContactUs';
import AboutUs from './AboutUs';
//import './ReactTable.css';
// // import Footer from './Footer';
//import Header from './Header';
import Card from './Card';
import ReactTable from './ReactTable';
// import UseRef from './UseRef';


function App() {
  return (
    <div className='app'>
      {/* <Header /> */}


      {/* <UseRef /> */}


      {/* <Card
        imageSrc="reactimg.png"
        title="React image"
        description="This is a sample card ."

      />
      <Card
        imageSrc="reactimg.png"
        title="React image"
        description="This is a sample card ."
      />

      <Card
        imageSrc="reactimg.png"
        title="React image"
        description="This is a sample card ."
      />  */}


      {/* <ReactTable />  */}
      

      {/* <Footer />  

      {/* <UseRefINClass/> */}
      
    
      <BrowserRouter>
      <Navigation>
        <switch>
          <Routes>
            <Route path='/home' Component={Home}></Route>
            <Route path='/ContactUs' Component={ContactUs}></Route>
            <Route path='/AboutUs' Component={AboutUs}></Route>
            <Route path='/Card' Component={Card}></Route>
            <Route path='/ReactTable' Component={ReactTable}></Route>
          </Routes>
        </switch>
      </Navigation>
      </BrowserRouter>
      

    </div>
  );
}

// import Functionalcom from './Functionalcom';
// import StatesC from './StatesC';
// import Card from './Card';
// import StatesF from './StatesF';
// function App() {
//   return (
//     <div className="App">
//       <h1>Welcome to React Learning</h1>

//       <Functionalcom name="react" />
//       <Functionalcom name="node" />
//       <Card
//         imageSrc="reactimg.png"
//         title="React image"
//         description="This is a sample card ."
//         button="Ok"
//       />


//       <StatesF />
//       <StatesC />

//     </div>


//   );
// }

export default App;
//<Functionalcom name="react" />
//<Functionalcom name="node" />