// Card.js

import React from 'react';
import myimage from './myimage/reactimg.png';
import './App.css';
function Card(props) {
  return (
    <div className="card">
      <img src={myimage}  alt={props.title} />
      <div className="card-body">
        <h2>{props.title}</h2>
        <p>{props.description}</p>
        <button>{props.button}Submit</button>
      </div>
    </div>

    

  


  );
}

export default Card;
