import React, { Component } from 'react'

class UseRefINClass extends Component {


    constructor(props) {
        super(props);
        this.name = React.createRef();

    }
    componentDidMount() {           //without re rendering of a dom elements to perform some action
        this.name.current.focus();
    }
    handleFocus=()=> {
    this.name.current.focus();
}

render() {
    return (
        <div>
            <input type='text' ref={this.name}></input>
            <button onClick={this.handleFocus}>click</button>
        </div>
    )
}
}

export default UseRefINClass