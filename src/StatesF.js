import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.css';

function StatesF() {

    const [count, setCount] = useState(0);
    const [operation, setOperation] = useState("operation");


    const handleIncrement = () => {
        setCount(count + 1);
        setOperation("increasing");
    }

    const handleDecrement = () => {
        setCount(count - 1);
        setOperation("decreasing");
    }
    return (
        <div>
            <h1>{count}</h1>
            <h1>{operation}</h1>
            <button onClick={handleIncrement} className='btn btn-danger'>Increment</button>
            <button onClick={handleDecrement}>Decrement</button>

        </div>
    )
}
export default StatesF
