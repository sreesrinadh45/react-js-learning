import React, { Component } from 'react'

export class StatesC extends Component {

    constructor(props){
        super(props)
        this.state = {
            count :0,
            name: "operation"
        }
      
    }
    hancleIncrement = () =>{
        this.setState({
            count:this.state.count+1,
            name:"incrementing"
        })
     }
  render() {
    return (
      <div>
        <h1>{this.state.count}</h1>
        <h1>{this.state.name}</h1>
        <button style={{color:"red"}} onClick={this.hancleIncrement}>Increment</button>
      </div>
    )
  }
}

export default StatesC
