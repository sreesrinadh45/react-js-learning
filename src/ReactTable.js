import React from "react";
import './App.css';


const data = [
  {ID: 23333,Name:"sai",Batch:23, Performance:"male", },
  {ID: 23353,Name:"srinadh",Batch:22, Performance:"male", },
  {ID: 23333,Name:"ram",Batch:23, Performance:"male", },
  {ID: 23333,Name:"govardhan",Batch:23, Performance:"male", },
]
 function ReactTable(){
         return(
            <div className="ReactTable">
              <h1>Talent Sprint Student Data</h1>
              <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Batch</th>
                        <th>Perfomance</th>
                    </tr>
                </thead>
              </table>
              <tbody>
               {
                data.map((value, key)=>{
                  return(
                    <tr key={key}>
                      <td>{value.ID}</td>
                      <td>{value.Name}</td>
                      <td>{value.Batch}</td>
                      <td>{value.Performance}</td>
                    </tr>
                  )
                })
               }

              </tbody>
            </div>
         )
 }
 export default ReactTable;