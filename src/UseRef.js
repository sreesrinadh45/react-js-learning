import React, { useEffect, useRef, useState } from "react";
const UseRef = () => {
    const [name , setName] = useState('');

    const username = useRef();
    const useremail = useRef();
    const count = useRef(0);

     useEffect(()=>{
        (username.current.focus());
     })
    const handleFocus = () => {
        console.log(username.current.value);
        console.log(useremail.current.value);
        (username.current.focus());
    }
    const handlIncrement = () =>{
        console.log(count.current = count.current+1);
    }

    return (
        <div>
            <input type="text" placeholder="Name" onChange={(e) => { setName(e.target.value) }} ref={username}></input>
            <input type="text" placeholder="email" ref={useremail}></input>
            <button onClick={handleFocus}>Submit</button>
            <button onClick={handlIncrement}>Increment</button>
        </div>
    )
}
export default UseRef;